# chip8 emu

Chip 8 emulator written in rust. Created by following: https://github.com/aquova/chip8-book.

## Installation
`git clone https://gitlab.com/zefrof/chip8-emu.git`

`cd chip8-emu`

`cargo build`

## Usage
`cargo run path/to/rom`

## Roadmap
http://devernay.free.fr/hacks/chip8/C8TECH10.HTM

## License
AGPLv3
